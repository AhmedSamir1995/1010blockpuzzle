﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
[Serializable]
public class Shape
{
    [SerializeField]
    public Vector2Int[] tilesPositions;

    public Vector2Int[] tilesRelativeTo(Vector2Int position)
    {
        Vector2Int[] tilesPositions = this.tilesPositions.Clone() as Vector2Int[];
        for (int i = 0; i < tilesPositions.Length; i++)
        {
            tilesPositions[i] += position;
        }
        return tilesPositions;

    }

    //Static
    static List<Shape> shapes;
    public static List<Shape> Shapes
    {
        get => shapes;
    }
    static Shape()
    {
        shapes = new List<Shape>();
    }


    Shape()
    {
        shapes.Add(this);
        Debug.Log("Shapes count= " + shapes.Count);
    }
}
