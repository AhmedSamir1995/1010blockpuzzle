﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileColorModifier : TileModifier
{
    public Color[] colorPalette;
    protected override void ModifierFunction(Tile[] tiles)
    {
        if (colorPalette != null && colorPalette.Length > 0)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].SetColor(colorPalette[Random.Range(0, colorPalette.Length)]);
            }
        }
    }
}
