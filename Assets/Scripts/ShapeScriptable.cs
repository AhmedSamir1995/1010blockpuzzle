﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="Blocks\\Create New Shape",fileName ="Shape")]
public class ShapeScriptable : ScriptableObject
{
    public Shape shape;

}
