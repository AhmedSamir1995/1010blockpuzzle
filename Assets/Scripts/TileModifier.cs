﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class TileModifier : MonoBehaviour
{
    public TileModifier nextModifier;
    public void ApplyModifier(Tile[] tiles)
    {
        ModifierFunction(tiles);
        if(nextModifier)

        nextModifier.ApplyModifier(tiles);
    }

    protected abstract void ModifierFunction(Tile[] tiles);
}
