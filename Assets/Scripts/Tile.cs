﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{

    public SpriteRenderer mySpriteRenderer;

    public void SetColor(Color color)
    {
        mySpriteRenderer.color = color;
    }
    public void SetSprite(Sprite sprite)
    {
        mySpriteRenderer.sprite = sprite;
    }

    public void DoActionToSpriteRenderer(System.Action<SpriteRenderer> action)
    {
        action.Invoke(mySpriteRenderer);
    }

    public void SetSize(float size)
    {
        transform.localScale = Vector3.one * size;
    }

    public void SetPosition(float x, float y)
    {
        SetPosition(new Vector2(x, y));
    }
    public void SetPosition(Vector2 position)
    {
        transform.position = position;
    }
}
