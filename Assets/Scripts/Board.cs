﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    [SerializeField]
    Vector2Int boardSize;
    [SerializeField]
    float tileSize;
    public Tile[,] boardTiles;
    public Tile tilePrefab;
    public Color defaultTileColor;
    public Color highlightedColor;
    public SpriteRenderer border;
    public float borderMargin;

    public ShapeScriptable activeShape;
    // Start is called before the first frame update
    void Start()
    {
        boardTiles = new Tile[boardSize.x, boardSize.y];
        print(boardTiles.Length);
    }
    Vector2Int lastHighlighted;
    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            Vector2Int tilePosition = MousePositionToTilePosition(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            //exitConditions
            if (TileExist(tilePosition))
            {
                if (tilePosition != lastHighlighted)
                {
                    print("MousePosition: " + Camera.main.ScreenToWorldPoint(Input.mousePosition) +
                        ", TilePosition:" + tilePosition);
                    UnHighlight(lastHighlighted);
                    Highlight(tilePosition);
                    //boardTiles[tilePosition.x, tilePosition.y].SetColor(highlightedColor);
                    //boardTiles[lastHighlighted.x, lastHighlighted.y].SetColor(defaultTileColor);
                    lastHighlighted = tilePosition;
                }
            }
        }

    }
    public void InitializeBoard()
    {
        border.size = (Vector2)boardSize * (tileSize+borderMargin/2);
        for(int i=0;i<boardSize.x;i++)
        {
            for(int j=0;j<boardSize.y;j++)
            {
                if(!boardTiles[i,j])
                boardTiles[i,j] = GameObject.Instantiate(tilePrefab,transform);
                boardTiles[i, j].SetSize(tileSize);
                boardTiles[i, j].SetPosition((transform.position.x - ((boardSize.x-1) * tileSize) / 2) + i * tileSize,
                                             (transform.position.y - ((boardSize.y-1) * tileSize) / 2) + j * tileSize);
                boardTiles[i, j].SetColor(defaultTileColor);
            }
        }
    }

    public Vector2Int MousePositionToTilePosition(Vector2 mousePosition)
    {
        Vector2Int tilePosition = Vector2Int.zero;
        //tilePosition.x = 0;
        //To get i Reversing this fn (transform.position.x - ((boardSize.x-1) * tileSize) / 2) + i * tileSize

        tilePosition.x = Mathf.RoundToInt(mousePosition.x - (transform.position.x - ((boardSize.x - 1) * tileSize) / 2) / tileSize);

        //tilePosition.y = 0;

        tilePosition.y = Mathf.RoundToInt(mousePosition.y - (transform.position.y - ((boardSize.y - 1) * tileSize) / 2) / tileSize);
        return tilePosition;
    }

    void Highlight(Vector2Int tilePosition)
    {
        Vector2Int[] toBeHighlightedTiles= activeShape.shape.tilesRelativeTo(tilePosition);
        for(int i=0;i<toBeHighlightedTiles.Length;i++)
        {
            if(TileExist(toBeHighlightedTiles[i]))
                boardTiles[toBeHighlightedTiles[i].x, toBeHighlightedTiles[i].y].SetColor(highlightedColor);
        }
    }

    void UnHighlight(Vector2Int tilePosition)
    {
        Vector2Int[] toBeHighlightedTiles = activeShape.shape.tilesRelativeTo(tilePosition);
        for (int i = 0; i < toBeHighlightedTiles.Length; i++)
        {
            if (TileExist(toBeHighlightedTiles[i]))
                boardTiles[toBeHighlightedTiles[i].x, toBeHighlightedTiles[i].y].SetColor(defaultTileColor);
        }
    }

    public bool TileExist(Vector2Int tilePosition)
    {
        return boardTiles.GetLength(0) > tilePosition.x && tilePosition.x >= 0 && boardTiles.GetLength(1) > tilePosition.y && tilePosition.y >= 0;
    }
}
