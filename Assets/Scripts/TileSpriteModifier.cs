﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileSpriteModifier : TileModifier
{
    public Sprite sprite;

    protected override void ModifierFunction(Tile[] tiles)
    {
        if (sprite)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                tiles[i].SetSprite(sprite);
            }
        }
    }
}
