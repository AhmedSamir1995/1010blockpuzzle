﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Tile tile;

    public Color[] colorPallette;

    public Shape[] shapes;

    public bool[,] board= new bool[10,10];

    
    // Start is called before the first frame update
    void Start()
    {
        Resources.LoadAll<ShapeScriptable>("");
        shapes = Shape.Shapes.ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Shape GetRandomShape()
    {
        return shapes[Random.Range(0, shapes.Length)];
    }

    public Color GetRandomColor()
    {
        return colorPallette[Random.Range(0, colorPallette.Length)];
    }


}
