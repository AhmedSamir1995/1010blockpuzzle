﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ShapeEditor : EditorWindow
{
    string myString = "Hello World";
    bool groupEnabled;
    bool myBool = true;
    float myFloat = 1.23f;

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/ShapeEditor")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        ShapeEditor window = (ShapeEditor)EditorWindow.GetWindow(typeof(ShapeEditor));
        window.Show();
    }

    Texture2D whiteTexture;

    ShapeScriptable[] shapes;
    private void OnEnable()
    {
        if (EditorPrefs.HasKey("ObjectPath"))
        {
            string objectPath = EditorPrefs.GetString("ObjectPath");
            shapes = Selection.GetFiltered<ShapeScriptable>(SelectionMode.DeepAssets);
            shapePos = new bool[3, 3];
            //shape = (AssetDatabase.LoadAssetAtPath(objectPath, typeof(ShapeScriptable)) as ShapeScriptable);
            //inventoryItemList = AssetDatabase.LoadAssetAtPath(objectPath, typeof(InventoryItemList)) as InventoryItemList;
            
        }

        whiteTexture = (AssetDatabase.LoadAssetAtPath("Assets/Sprites/Asset 4@2x.png", typeof(Texture)) as Texture) as Texture2D;

    }
    Color guiColor;
    bool[,] shapePos;
    public int selectedIndex;
    void OnGUI()
    {
        guiColor = GUI.color;
        GUILayout.Label("Shape", EditorStyles.boldLabel);

        GUILayout.Label(shapes.Length.ToString(), EditorStyles.boldLabel);
        myString = EditorGUILayout.TextField("Text Field", shapes[selectedIndex].name);


        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                shapePos[i, j] = true;
            }
        }
        for(int i=0;i<shapes[selectedIndex].shape.tilesPositions.Length;i++)
        {
            shapePos[shapes[selectedIndex].shape.tilesPositions[i].x, 2-shapes[selectedIndex].shape.tilesPositions[i].y] = false;
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("<"))
        {
            selectedIndex+=shapes.Length-1;
            selectedIndex %= shapes.Length;
        }
        if (GUILayout.Button(">"))
        {
            selectedIndex += shapes.Length + 1;
            selectedIndex %= shapes.Length;
        }
        GUILayout.EndHorizontal();
        // shapePos[0,0] = GUILayout.Toggle(shapePos[0,0], AssetDatabase.LoadAssetAtPath("Assets/Sprites/Asset 4@2x.png", typeof(Texture)) as Texture);
        // GUILayout.SelectionGrid(0, images: new Texture[] { AssetDatabase.LoadAssetAtPath("Assets/Sprites/Asset 4@2x.png", typeof(Texture)) as Texture, AssetDatabase.LoadAssetAtPath("Assets/Sprites/Asset 4@2x.png", typeof(Texture)) as Texture, AssetDatabase.LoadAssetAtPath("Assets/Sprites/Asset 4@2x.png", typeof(Texture)) as Texture },6);
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUI.color = shapePos[0, 0] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[0,0] = !shapePos[0, 0];

        GUI.color = shapePos[1, 0] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[1, 0] = !shapePos[1, 0];

        GUI.color = shapePos[2, 0] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[2, 0] = !shapePos[2, 0];

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();

        GUI.color = shapePos[0, 1] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[0, 1] = !shapePos[0, 1];

        GUI.color = shapePos[1, 1] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[1, 1] = !shapePos[1, 1];

        GUI.color = shapePos[2, 1] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[2, 1] = !shapePos[2, 1];

        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();

        GUI.color = shapePos[0, 2] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[0, 2] = !shapePos[0, 2];

        GUI.color = shapePos[1, 2] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[1, 2] = !shapePos[1, 2];

        GUI.color = shapePos[2, 2] ? Color.black : Color.blue;
        if (GUILayout.Button(whiteTexture, GUILayout.Height(100), GUILayout.Width(100)))
            shapePos[2, 2] = !shapePos[2, 2];

        GUILayout.EndHorizontal();
        GUILayout.EndVertical();
        GUI.color = guiColor;
        EditorGUILayout.BeginToggleGroup("Optional Settings", groupEnabled);
        myBool = EditorGUILayout.Toggle("Toggle", myBool);
        myFloat = EditorGUILayout.Slider("Slider", myFloat, -3, 3);
        List<Vector2Int> vector2Ints=new List<Vector2Int>();
        for(int i=0;i< 3;i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (!shapePos[i, j])
                    vector2Ints.Add(new Vector2Int(i, 2-j));

            }
        }
        shapes[selectedIndex].shape.tilesPositions = vector2Ints.ToArray();

        EditorGUILayout.EndToggleGroup();
    }
}
